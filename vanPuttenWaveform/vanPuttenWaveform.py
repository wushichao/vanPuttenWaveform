""" implementation of the van Putten exponential spindown waveform model

Copyright (C) 2018 David Keitel

Code for calculating gravitational waveforms following
the 'exponential anti-chirp' track
suggested by M. van Putten in https://www.arxiv.org/abs/1806.02165

code based on the millisecond magnetar model
implementation by Paul Lasky
"""

from __future__ import division, print_function

import numpy as np

import astropy.constants as cc
import astropy.units as uu

def fgw ( time, f0, ts, fs, tau ):
    """ gravitational-wave frequency as function of time

    exponential spindown model from
    Eq. (2) of van Putten https://arxiv.org/abs/1806.02165

    Args:
        time: time array [s]
        f0: asymptotic gravitational-wave frequency at end of track [Hz]
        ts: start time of exponential track [s]
        fs: starting frequency of exponential track [Hz]
        tau: e-fold spindown timescale [s]

    Returns:
        gw frequency timeseries [Hz]
    """

    return np.heaviside(time-ts,1) * ( (fs-f0) * np.exp(-(time-ts)/tau) + f0 )

def h0(time, f0, ts, fs, tau, eps, dd, II = 1e45):
    """ gravitational-wave amplitude as function of time

    h(t)=4\pi^2 G II eps fgw(t) / (c^4 dd)

    Args:
        time: time array [s]
        f0: asymptotic gravitational-wave frequency at end of track [Hz]
        ts: start time of exponential track [s]
        fs: starting frequency of exponential track [Hz]
        tau: e-fold spindown timescale [s]
        eps: ellipticity of the star [dimensionless]
        dd: distance [Mpc]
        II: principal moment of inertia [g cm^2]

    Returns:
        hh:   strain amplitude timeseries [dimensionless]
        f_gw: gw frequency timeseries [Hz]
    """

    f_gw = fgw ( time, f0, ts, fs, tau )  # create the frequency array

    # get the units right:
    f_gw = f_gw * uu.Hz
    II = II * uu.g * uu.cm**2
    dd = dd * uu.Mpc

    consts = 4. * np.pi**2 * cc.G / cc.c**4

    # calculate the strain, and convert to cgs units
    # should be dimensionless, so this is a sanity check
    hh = (consts * II * eps * f_gw**2 / dd).cgs

    return hh, f_gw

def ht(time, f0, ts, fs, tau, eps, dd, cosi, II = 1e45):
    """ gravitational-wave strain time series as a function of time

    This calculates the phase and the amplitude in both GW polarizations.
    I.e. all that's needed for a waveform generator.

    Args:
        time: time array [s]
        f0: asymptotic gravitational-wave frequency at end of track [Hz]
        ts: start time of exponential track [s]
        fs: starting frequency of exponential track [Hz]
        tau: e-fold spindown timescale [s]
        eps: ellipticity of the star [dimensionless]
        dd: distance [Mpc]
        cosi: cosine of inclination angle
        II: principal moment of inertia [g cm^2]

    Returns:
        dphi: phase timeseries [dimensionless]
        ap: amplitude timeseries (plus polarization) [dimensionless]
        ax: amplitude timeseries (cross polarization) [dimensionless]
    """

    # Signal switches on at time=0
    if time < 0:
        return 0, 0, 0

    h_0, f_gw = h0(time, f0, ts, fs, tau, eps, dd, II) # Calc GW strain array and frequency

    # phase (f_gw integrated)
    dphi = 2.*np.pi * ( f0*time - tau*(fs-f0)*np.exp(-(time-ts)/tau) )

    ap = h_0 * (1. + cosi**2) / 2.   # a_plus
    ax = h_0 * cosi                  # a_cross

    return dphi, ap, ax

def waveform(f0, ts, fs, tau, eps, dd, cosi, II = 1e45):
    """ produce generator function for time-domain gravitational waveform

    Args:
        f0: asymptotic gravitational-wave frequency at end of track [Hz]
        ts: start time of exponential track [s]
        fs: starting frequency of exponential track [Hz]
        tau: e-fold spindown timescale [s]
        eps: ellipticity of the star [dimensionless]
        dd: distance [Mpc]
        cosi: cosine of inclination angle
        II: principal moment of inertia [g cm^2]

    Returns:
        wf: generator function suitable for use with lalpulsar.simulateCW
    """

    def wf(time):
        dphi, ap, ax = ht(time, f0, ts, fs, tau, eps, dd, cosi, II)
        return dphi, ap, ax

    return wf

def Erot(f0, II=1e45):
    """ initial total rotational energy

    e.g. see Eq.(11) of Sarin et al, https://arxiv.org/abs/1805.01481v1
    Note: Omega=pi*fgw=2*pi*frot

    Args:
        f0: initial gravitational-wave frequency [Hz]
        II: principal moment of inertia [g cm^2]

    Returns:
        Erot: rotational energy [erg]
    """

    # get the units right:
    f0 = f0 / uu.s
    II = II * uu.g * uu.cm**2

    Erot = 0.5 * II * f0**2. * np.pi**2.

    return Erot.cgs

def Egw(t, f0, ts, fs, tau, eps, II=1e45):
    """ emitted gravitational wave energy Egw(t) up to time t

    Egw = Integral_{t=0}^{T} dt (32G/(5c^5)) Izz^2 eps^2 Omega^6(t)

    Args:
        f0: asymptotic gravitational-wave frequency at end of track [Hz]
        ts: start time of exponential track [s]
        fs: starting frequency of exponential track [Hz]
        tau: e-fold spindown timescale [s]
        eps: ellipticity of the star [dimensionless]
        II:  principal moment of inertia [g cm^2]

    Returns:
        Egw: emitted energy [erg]
    """

    # get the units right:
    t = t * uu.s
    f0 = f0 / uu.s
    ts = ts * uu.s
    fs = fs / uu.s
    tau = tau * uu.s
    II = II * uu.g * uu.cm**2

    # integrated in Mathematica as:
    # Integrate[((fs - f0) Exp[-(t - ts)/\[Tau]] + f0)^6, {t, ts, t}]
    f_integral = f0**6*(t-ts) + tau*(
                   + 6.0      * (np.exp(-   (t-ts)/tau) - 1.) * f0**5*(f0-fs)
                   - (15./2.) * (np.exp(-2.*(t-ts)/tau) - 1.) * f0**4*(f0-fs)**2
                   + (20./3.) * (np.exp(-3.*(t-ts)/tau) - 1.) * f0**3*(f0-fs)**3
                   - (15./4.) * (np.exp(-4.*(t-ts)/tau) - 1.) * f0**2*(f0-fs)**4
                   + ( 6./5.) * (np.exp(-5.*(t-ts)/tau) - 1.) * f0   *(f0-fs)**5
                   - ( 1./6.) * (np.exp(-6.*(t-ts)/tau) - 1.) *       (f0-fs)**6
                 )

    prefactor = 32.*np.pi**6*cc.G/(5.*cc.c**5.)
    Egw = prefactor * II**2. * eps**2. * f_integral

    return Egw.cgs
